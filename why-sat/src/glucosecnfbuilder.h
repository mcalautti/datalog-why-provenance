#pragma once

#include "cnfbuilder.h"

#include <core/Solver.h>
#include <core/SolverTypes.h> 
//#include <parallel/ParallelSolver.h>
//#include <parallel/MultiSolvers.h>

#include <utils/System.h>
#include <utils/ParseUtils.h>
#include <utils/Options.h>

//#include <propagators/utils/SATGraph.h>
//#include <propagators/AcyclicityPropagator.h>
//#include <propagators/GeneralizedPropagator.h>

namespace dexp {

class glucosecnfbuilder : public cnfbuilder {

public:
    glucosecnfbuilder();
    virtual ~glucosecnfbuilder() {}

    virtual cnfbuilder& set_cnf_info(size_t num_vars, size_t num_clauses, const std::vector<atom_graph>& components) override;

    virtual cnfbuilder& begin_clause() override;
    virtual cnfbuilder& end_clause() override;
    virtual cnfbuilder& add_pos_literal(int var_id) override;
    virtual cnfbuilder& add_neg_literal(int var_id) override;
    
    void use_builtin_acyclicity(bool value) { m_builtin = value; }
    
    Glucose::Solver& solver();

private:

    Glucose::vec<Glucose::Lit> m_lits;

    /* GraphSAT specific objects*/
    //SATGraph m_satgraph;
    //std::unique_ptr<AcyclicityPropagator> m_propagator;
	
    //std::unique_ptr<Glucose::ParallelSolver> m_parallel_solver;
    Glucose::Solver m_current_solver;

    bool m_working_on_clause{};
    size_t m_num_literals_current_clause{};

    bool m_builtin;
};
}