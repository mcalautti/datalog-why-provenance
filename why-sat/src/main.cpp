#include "groundparser.h"
#include "satencoding.h"
#include "glucosecnfbuilder.h"
#include "timer.h"

#include <argparse/argparse.hpp>

#include <iostream>
#include <fstream>
#include <csignal>
#include <chrono>
#include <atomic>

static dexp::timer<std::chrono::microseconds,std::chrono::steady_clock> timer;

void add_data(int64_t data){
    std::cout << data << " " << std::flush;
}

void enumerate_models(  Glucose::Solver& solver,
                        int num_models,
                        const dexp::db_vars_map& db_vars_map,
                        bool must_print_models) {

    std::vector<std::vector<int>> models;
    timer.tick();

    if(!solver.simplify() || !solver.okay()){
        timer.tock();
        add_data(timer.duration().count());
    }else{

        int model_count = 0;
        while(true) {
            Glucose::vec<Glucose::Lit> dummy;
            Glucose::lbool ret = solver.solveLimited(dummy);

            timer.tock();
            add_data(timer.duration().count());

            if (ret == l_True){

                timer.tick();

                Glucose::vec<Glucose::Lit> model_blocker;
                std::vector<int> model;

                for(auto& entry : db_vars_map) {
                    auto var_id = entry.first;
                    auto& fact_name = entry.second;

                    if(solver.model[var_id] == l_True) {
                        if(must_print_models){
                            std::cout << fact_name << " ";
                        }

                        model.push_back(var_id);
                        //model_blocker.push(mkLit(var_id,true));
                    }
                    model_blocker.push(Glucose::mkLit(var_id,solver.model[var_id] == l_True));
                }

                if(must_print_models){
                    std::cout << '!' << std::endl;
                }

                solver.addClause_(model_blocker);
                models.push_back(model);

                model_count++;
                if(num_models > 0 && model_count >= num_models){
                    break;
                }
            }else{
                break;
            }
        }
    }
}

struct cli_args : public argparse::Args {
    std::string& goal    = kwarg("g,goal", "The query goal fact");
    int& num_supports    = kwarg("n,num-supports", "Number of supports to generate (0 means all)").set_default(0);
    bool& print          = flag("p,print", "If specified, prints the supports");

    std::optional<std::string>& downward_file = 
    arg("The file containing the downward closure built with DLV (if not specified, stdin will be used)");
};

int main(int argc, char* argv[]){

    try {
        auto args = argparse::parse<cli_args>(argc,argv,true);

        //Exit gracefully, when asked to terminate
        //std::signal(SIGTERM, signal_handler);
        //std::signal(SIGINT, signal_handler);
        
        timer.tick();

        dexp::ground_parser parser;
        dexp::pre_graph grounding_graph;

        size_t num_nodes = 0;
        size_t num_hyper_edges = 0;
        if (!args.downward_file.has_value()){ //Read from stdin
            parser.to_graph(std::cin, grounding_graph, num_nodes, num_hyper_edges);
        } else{ //Read from file
            std::ifstream in(args.downward_file.value());

            if(!in){
                std::cerr << "Cannot open file " << args.downward_file.value() << std::endl;
                return -1;
            }

            parser.to_graph(in, grounding_graph, num_nodes, num_hyper_edges);
        }

        dexp::glucosecnfbuilder builder;
        builder.use_builtin_acyclicity(false);

        dexp::sat_encoding encoding(builder);
        encoding.set_acyclicity_mode(dexp::sat_encoding::acyclicity_mode::vertex_elimination);
        
        encoding.set_goal(args.goal);

        encoding.build(grounding_graph);
        
        //Measure time to build the formula
        timer.tock();
        auto formula_build_time = timer.duration().count();

        auto& solver = builder.solver();

        add_data(num_nodes);
        add_data(num_hyper_edges);
        add_data(solver.nVars());
        add_data(solver.nClauses());
        add_data(formula_build_time);

        enumerate_models(solver, args.num_supports, encoding.vars_to_db_map(), args.print);

    } catch (const std::runtime_error &e) {
        std::cerr << e.what() << std::endl;
        std::cerr << "Please use --help for usage instructions" << std::endl;
        return -1;
    }

    return 0;
} 
