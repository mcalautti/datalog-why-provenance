## why-sat
This directory contains the source code of why-sat. In what follows we will give instructions on how to compile why-sat on Linux.

### Install dependencies
why-sat requires very few dependencies. That is, a compiler (g++), CMake, and the zlib library. You can install them like:
- `sudo apt install build-essential cmake zlib1g-dev` (Debian and Ubuntu based)
- `sudo dnf install g++ make cmake zlib-devel` (Fedora)

### Building
Open a terminal inside the "why-sat" directory (i.e., where this README is located) and type:

- `cmake -B build -S . -DCMAKE_BUILD_TYPE=Release`

The above will fetch all the needed dependencies and configure the project. 

Then, compile with:

- `cmake --build build --config Release`

After compilation completes, the why-sat binary can be found in the "build" directory.

Although the binary can be executed on its own, we suggest running it via the tooling present in the "experiments" directory, as explained in the README in the root of this repository.